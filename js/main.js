  (function () {

    //******************************************************/
    //******************** G L O B A L  ********************/
    //******************************************************/

    let kartenSet = {
      kreuz_as: {
        Wert: 11,
        img: 'img/ace_of_clubs.svg'
      },
      kreuz_zwei: {
        Wert: 2,
        img: 'img/2_of_clubs.svg'
      },
      kreuz_drei: {
        Wert: 3,
        img: 'img/3_of_clubs.svg'
      },
      kreuz_vier: {
        Wert: 4,
        img: 'img/4_of_clubs.svg'
      },
      kreuz_fuenf: {
        Wert: 5,
        img: 'img/5_of_clubs.svg'
      },
      kreuz_sechs: {
        Wert: 6,
        img: 'img/6_of_clubs.svg'
      },
      kreuz_sieben: {
        Wert: 7,
        img: 'img/7_of_clubs.svg'
      },
      kreuz_acht: {
        Wert: 8,
        img: 'img/8_of_clubs.svg'
      },
      kreuz_neun: {
        Wert: 9,
        img: 'img/9_of_clubs.svg'
      },
      kreuz_zehn: {
        Wert: 10,
        img: 'img/10_of_clubs.svg'
      },
      kreuz_bube: {
        Wert: 10,
        img: 'img/jack_of_clubs.svg'
      },
      kreuz_dame: {
        Wert: 10,
        img: 'img/queen_of_clubs.svg'
      },
      kreuz_koenig: {
        Wert: 10,
        img: 'img/king_of_clubs.svg'
      },
      karo_as: {
        Wert: 11,
        img: 'img/ace_of_diamonds.svg'
      },
      karo_zwei: {
        Wert: 2,
        img: 'img/2_of_diamonds.svg'
      },
      karo_drei: {
        Wert: 3,
        img: 'img/3_of_diamonds.svg'
      },
      karo_vier: {
        Wert: 4,
        img: 'img/4_of_diamonds.svg'
      },
      karo_fuenf: {
        Wert: 5,
        img: 'img/5_of_diamonds.svg'
      },
      karo_sechs: {
        Wert: 6,
        img: 'img/6_of_diamonds.svg'
      },
      karo_sieben: {
        Wert: 7,
        img: 'img/7_of_diamonds.svg'
      },
      karo_acht: {
        Wert: 8,
        img: 'img/8_of_diamonds.svg'
      },
      karo_neun: {
        Wert: 9,
        img: 'img/9_of_diamonds.svg'
      },
      karo_zehn: {
        Wert: 10,
        img: 'img/10_of_diamonds.svg'
      },
      karo_bube: {
        Wert: 10,
        img: 'img/jack_of_diamonds.svg'
      },
      karo_dame: {
        Wert: 10,
        img: 'img/queen_of_diamonds.svg'
      },
      karo_koenig: {
        Wert: 10,
        img: 'img/king_of_diamonds.svg'
      },
      herz_as: {
        Wert: 11,
        img: 'img/ace_of_hearts.svg'
      },
      herz_zwei: {
        Wert: 2,
        img: 'img/2_of_hearts.svg'
      },
      herz_drei: {
        Wert: 3,
        img: 'img/3_of_hearts.svg'
      },
      herz_vier: {
        Wert: 4,
        img: 'img/4_of_hearts.svg'
      },
      herz_fuenf: {
        Wert: 5,
        img: 'img/5_of_hearts.svg'
      },
      herz_sechs: {
        Wert: 6,
        img: 'img/6_of_hearts.svg'
      },
      herz_sieben: {
        Wert: 7,
        img: 'img/7_of_hearts.svg'
      },
      herz_acht: {
        Wert: 8,
        img: 'img/8_of_hearts.svg'
      },
      herz_neun: {
        Wert: 9,
        img: 'img/9_of_hearts.svg'
      },
      herz_zehn: {
        Wert: 10,
        img: 'img/10_of_hearts.svg'
      },
      herz_bube: {
        Wert: 10,
        img: 'img/jack_of_hearts.svg'
      },
      herz_dame: {
        Wert: 10,
        img: 'img/queen_of_hearts.svg'
      },
      herz_koenig: {
        Wert: 10,
        img: 'img/king_of_hearts.svg'
      },
      pique_as: {
        Wert: 11,
        img: 'img/ace_of_spades.svg'
      },
      pique_zwei: {
        Wert: 2,
        img: 'img/2_of_spades.svg'
      },
      pique_drei: {
        Wert: 3,
        img: 'img/3_of_spades.svg'
      },
      pique_vier: {
        Wert: 4,
        img: 'img/4_of_spades.svg'
      },
      pique_fuenf: {
        Wert: 5,
        img: 'img/5_of_spades.svg'
      },
      pique_sechs: {
        Wert: 6,
        img: 'img/6_of_spades.svg'
      },
      pique_sieben: {
        Wert: 7,
        img: 'img/7_of_spades.svg'
      },
      pique_acht: {
        Wert: 8,
        img: 'img/8_of_spades.svg'
      },
      pique_neun: {
        Wert: 9,
        img: 'img/9_of_spades.svg'
      },
      pique_zehn: {
        Wert: 10,
        img: 'img/10_of_spades.svg'
      },
      pique_bube: {
        Wert: 10,
        img: 'img/jack_of_spades.svg'
      },
      pique_dame: {
        Wert: 10,
        img: 'img/queen_of_spades.svg'
      },
      pique_koenig: {
        Wert: 10,
        img: 'img/king_of_spades.svg'
      }
    };


    sounds = {      // Objekt mit den URLs der Sounds
      wel      : 'sounds/welcome.wav',
      jetons   : 'sounds/jetons_setzen.wav',
      win      : 'sounds/jackpot.wav',
      loos     : 'sounds/fail.wav',   
      draw     : 'sounds/draw1.mp3',
      playerBj : 'sounds/blackjack2.wav',
      dealerBj : 'sounds/dealer_blackjack.wav',
      card     : 'sounds/karte_nehmen.wav'
    
  };



    let startDeck = []; // leeres Array für die keys des Kartensets als Referenz 
    let dealer = []; // Array für die Karten des Dealers
    let spieler = []; // Array für die Karten des Spielers
    let werteSpieler = 0; // Summer der Kartenwerte des Spielers
    let werteDealer = 0; // Summer der Kartenwerte des Dealers
    let gewinn = 0;
    let index = 1; // Zähler für Karten-IDs
    let flag = true; // steuert Willkommens-Overlay
    let flag2 = true;   // steuert das erscheinen des 'Einsatzbestaetigen'Buttons

    let einsatz = 0; //Vorsicht globale Variable
    let balance = 20; // Guthaben des Spielers bei Spielbeginn





    //********************************************************************/
    //********************   VARIABLEN und Funktionen ********************/
    //********************************************************************/


function noScrollDown() {       // unterbindet das Scrollen, wenn oben
  window.scrollTo(0, 0);
};

function noScrollUp() {       // unterbindet das Scrollen, wenn unten
  window.scrollTo(0, 1500);
};

function playSound(path){       // zum Abspielen der Sounds
  audio = new Audio();
  audio.src = path;
  audio.play();
};



    function init() { // Alles auf Anfang
      startDeck = [];
      dealer = [];
      spieler = [];
      werteSpieler = 0;
      werteDealer = 0;
      einsatz = 0;
      index = 1; // Zähler für Karten-IDs
      flag2 = true;


      window.addEventListener('scroll', noScrollDown); // Browserfenster lässt sich nicht nach unten scrollen

      el('#bet').removeAttribute('disabled'); // Einsatz-Button aktivieren

      if (flag) { // abfrage flag; Willkommens-Overlay soll nur beim ersten Spielstart bzw. Website-Aufruf erscheinen.

        let overlay = document.createElement('div'); // erzeugen eines HTML-ELements, das sich über die Spieloberfläche legt und den Spieler über den Spielausgang informiert 
        overlay.setAttribute('id', 'overlay');
        overlay.setAttribute('class', 'tryAgain flat');
        overlay.setAttribute('value', 'Try Again');

        let text = document.createElement('p'); // erzeugen eines HTML-p, der innerhlab des overlays erscheint und Text aufnehmen kann
        text.setAttribute('id', 'overlay');
        text.setAttribute('class', 'overlay txt');
        text.innerHTML = 'Willkommen im Casino. <br><br>Wollen Sie eine Runde Blackjack spielen?';

        let button = document.createElement('button'); // erzeugen eines HTML-Buttons, der innerhlab des overlays erscheint
        button.setAttribute('id', 'centerbtn');
        button.setAttribute('class', 'btn');
        button.innerHTML = 'Platznehmen';

        overlay.appendChild(text); // erzeugte HTML Element werden dem Eltern-Element zugewiesen
        overlay.appendChild(button);
        el('body').appendChild(overlay);


        el('#centerbtn').addEventListener('click', function () { // entfernt bei Klick das Willkommens-Overlay
          playSound(sounds.wel);
          el('#overlay').remove();
          el('.game').setAttribute('style', ''); // entfernt die Styles blur und opacity aus game-div
        });

        flag = false;
      }



      pleite(); // prüft ob noch Guthaben vorhanden ist 

      startDeck = Object.keys(kartenSet); // Eigenschaft aus dem Objekt kartenSet auslesen und in das Array startDeck übergeben
      // console.log(startDeck);
      el('.ausgabeDeck').innerHTML = 'Noch ' + startDeck.length + ' Karten im Deck'; // gibt die verbliebene Anzahl an Karten im startDeck aus

      el('#balance').innerHTML = '<br>Balance: ' + balance + ' $';
      el('#spielhinweis').innerHTML = 'Beginne mit dem Setzen deines Einsatzes. ->';

      el('#deinEinsatz').innerHTML = '<b>Bitte mach deinen Einsatz.</b> <br><br> Dein Einsatz: ' + einsatz + ' $ <br> Du kannst noch ' + balance + ' $ setzen.';

      el('#spielerpunkte').innerHTML = 'Spieler Zahl';
      el('#dealerpunkte').innerHTML = 'Dealer Zahl';
      // el('#start').removeAttribute('disabled');
      el('#hit').setAttribute('disabled', 'disabled');
      el('#stay').setAttribute('disabled', 'disabled');

      el('.playBoxSpieler').innerHTML = '';
      el('.playBoxDealer').innerHTML = '';




    };

    function showHitStay() { // aktiviert Buttons bei Klick auf Start
      el('#hit').removeAttribute('disabled');
      el('#stay').removeAttribute('disabled');
    };


    // ### Folgende Funktionen haben wir für das Drag 'n Drop der Jetons aufgenommen und angepasst ###
    // ####################### gefunden bei w3school #######################
    function allowDrop(ev) {
      ev.preventDefault(); // unterbindet Standardfunktionen des Browsers
    }

    function drag(ev) {
      ev.dataTransfer.setData("text", ev.target.id); // übergibt den Typ und wert des gedraggten Elements
    }
    // ####################### gefunden bei codechannel.com #######################

    function drop(ev) { // Funktion auf unsere Bedürfnisse angepasst
      ev.preventDefault();
      var data = ev.dataTransfer.getData("text");
      //console.log(data);
      /* If you use DOM manipulation functions, their default behavior it not to copy but to alter and move elements. By appending a ".cloneNode(true)", you will not move the original element, but create a copy. */
      var nodeCopy = document.getElementById(data); // id aus dem img Element an nodeCopy übergeben

      let value;
      value = nodeCopy.getAttribute('value')

      if ((Number(value)) > balance) {
        // alert(balance + ' $ oder weniger setzen');

        if (balance === 0) {
          createButton('tryAgain_bottom flat', '<br>Du hast alles gesetzt, was du hast.', 'OK'); // Erstellt Overlay mit Textausgabe und weiter-Button
          el('#centerbtn').addEventListener('click', function () {
            el('#overlay').remove();
          });
        } else {
          createButton('tryAgain_bottom flat', balance + ' $ <br>oder weniger setzen', 'OK'); // Erstellt Overlay mit Textausgabe und weiter-Button
          el('#centerbtn').addEventListener('click', function () {
            el('#overlay').remove();
          });
        };

      } else {

        einsatz += Number(value); // Inhalt von nodeCopy in Zahl umgewnadelt und mit einsatz aufaddiert
        // console.log('Einsatz: ' + einsatz);
        // console.log('value: ' + value);

        balance -= Number(value); // einsatz wird vom Guthaben abgezogen
        // console.log('balan: ' + balance);

        el('#deinEinsatz').innerHTML = 'Dein Einsatz: ' + einsatz + ' $ <br> Du kannst noch ' + balance + ' $ setzen.';
        
        playSound(sounds.jetons);

        if(flag2){    // Button soll nur einmal erzeugt werden, daher flag-Schalter
          einsatzBestaetigenButton();   // erzeugt Button
          el('#betset').addEventListener('click', function(){   // Event Listener auf Button
            window.removeEventListener('scroll', noScrollUp);
            noScrollDown();
            window.addEventListener('scroll', noScrollDown);
            el('#betset').remove();
            einsatzMachen();
          });
          flag2 = false;
        };
      };


    }
    // ####################### ENDE gefunden bei codechannel.com #######################
    // ####################### ENDE gefunden bei w3school #######################


    function wertAs(kartenWert, speicher) {

      if (kartenWert === 11) { // prüft ob Ass

        if (speicher < 11) {
          return 11; // Ass zählt 11            
        } else {
          return 1; // Ass zählt 1        
        };

      } else { // wenn kein Ass, dann Kartenwert zurück
        return kartenWert; // der Kartenwert wird aus Objket kartenSet ermittelt und der Variablen werteSpieler aufaddieren
      };

    };

    // In der 'function gebenStart' wird abwechselnd dem Spieler und Dealer eine Karten aus dem StartDeck zugewiesen, bis beide zwei Karten haben, und diese Karten werden aus dem startDeck entfernt 
    function gebenStart() {

      let schalter = true; // Der schalter sorgt dafür, dass dem Spieler und dem Dealer abwechselnd eine Karte zugewiesen wird (spieler = true; dealer = false)

      for (let i = 0; i < 4; i++) {
        if (schalter) {
          let zufall = Math.floor(Math.random() * startDeck.length); // zufällige Zahl, die der Länge des startDecks-Arrays genommen wird
          spieler.push(startDeck[zufall]); // Wert aus startDeck wird an das Array spieler übergeben

          let temp = startDeck[zufall]; // Inhalt aus Array mit Index 'zufall' wird Variable temp zugewiesen 


          werteSpieler += wertAs(kartenSet[temp]['Wert'], werteSpieler); // der Kartenwert wird ermittelt und an die Funktion wertAs übergeben; Rückgabewert wird mit werteSpieler aufaddieren


          //console.log(werteSpieler);

          el('.ausgabeDeck').innerHTML = 'Noch ' + startDeck.length + ' Karten im Deck'; // gibt die verbliebene Anzahl an Karten im startDeck aus

          // createKarte(ele,id,cla,src,css)
          createKarte('img', 'card' + index + 'spieler', '', kartenSet[temp]['img'], '.playBoxSpieler'); // stellt die dem Spieler zugewiesenen Karten auf dem Spieltisch dar
          index++;

          startDeck.splice(zufall, 1); // Wert wird aus startDeck gelöscht
          schalter = false;

        } else {
          let zufall = Math.floor(Math.random() * startDeck.length); // zufällige Zahl, die zwischen 0 und der Länge des startDecks-Arrays liegen kann
          dealer.push(startDeck[zufall]); // zufällige Zahl wird als Index für das startDeck-Array genutzt, um einen beliebigen Wert aus dem Deck zu lesen und an das Array des Dealers zu übergeben

          let temp = startDeck[zufall]; // Inhalt aus Array mit Index 'zufall' wird Variable temp zugewiesen 
          werteDealer += wertAs(kartenSet[temp]['Wert'], werteDealer); // der Kartenwert wird ermittelt und an die Funktion wertAs übergeben; Rückgabewert wird mit werteDealer aufaddieren
          //console.log(werteDealer);

          el('.ausgabeDeck').innerHTML = 'Noch ' + startDeck.length + ' Karten im Deck'; // gibt die verbliebene Anzahl an Karten im startDeck aus

          switch (index) { // zweite Karte des Dealers soll verdeckt bleiben
            case 2:
              createKarte('img', 'card' + index + 'dealer', '', kartenSet[temp]['img'], '.playBoxDealer'); // stellt die dem Dealer zugewiesenen Karten auf dem Spieltisch dar
              index++;
              break;
            case 4:
              createKarte('img', 'card' + index + 'dealer', '', 'img/cardBack_1.svg', '.playBoxDealer'); // stellt die zweite dem Dealer zugewiesenen Karten VERDECKT auf dem Spieltisch dar
              index++;
              // console.log(dealer);
              break;
          };

          startDeck.splice(zufall, 1); // Wert wird aus startDeck gelöscht
          schalter = true;


        };

      };

      el('#dealerpunkte').innerHTML = kartenSet[dealer[0]]['Wert']; // Nur Wert der ersten Karte des Dealers wird angezeigt, zweite Karte bleibt vorerst verdeckt
      el('#spielerpunkte').innerHTML = werteSpieler; // Summe der Karten Werte des Spielers im DOM ausgeben 

      //console.log(dealer);
      //console.log(spieler);
    };


    function spielerHand() { // nimmt eine weitere Karte aus dem startDeck

      let zufall = Math.floor(Math.random() * startDeck.length);
      spieler.push(startDeck[zufall]);


      let temp = startDeck[zufall]; // Inhalt aus Array mit Index 'zufall' wird Variable temp zugewiesen 
      werteSpieler += wertAs(kartenSet[temp]['Wert'], werteSpieler); // der Kartenwert wird ermittelt und an die Funktion wertAs übergeben; Rückgabewert wird mit werteSpieler aufaddieren


      createKarte('img', 'card' + index + 'spieler', '', kartenSet[temp]['img'], '.playBoxSpieler'); // stellt die dem Spieler zugewiesenen Karten auf dem Spieltisch dar
      index++;

      playSound(sounds.card);


      startDeck.splice(zufall, 1);

      el('#spielerpunkte').innerHTML = werteSpieler;
      el('.ausgabeDeck').innerHTML = 'Noch ' + startDeck.length + ' Karten im Deck'; // gibt die verbliebene Anzahl an Karten im startDeck aus

      if (werteSpieler > 21) { // wenn der Spieler mehr als 21 Punkte hat, endet das Spiel an dieser Stelle
        el('#balance').innerHTML = 'Balance: ' + balance + ' $';
        el('#stay').setAttribute('disabled', 'disabled'); // Stellt sicher das die der stay Button nicht mehr klickbar ist
        el('#hit').setAttribute('disabled', 'disabled'); // das gleiche mit hit


        createButton('tryAgain flat', '<br>Du bist über 21 Punkte <br> Dein Einsatz ist futsch.', 'Weiter');

        playSound(sounds.loos);

        // el('#overlay').innerHTML = 'Du bist über 21 Punkte <br> Dein Einsatz ist futsch.' + pleite();
        el('#overlay').addEventListener('click', function () {
          el('#overlay').remove();
          init();
        });

      };
    };


    function dealerHand() { // Dealer ist am Zug; automatisch nach den Regeln für Dealer werden weitere Karten aus dem StartDeck genommen oder eben nicht

      el('#card4dealer').setAttribute('src', kartenSet[dealer[1]]['img']);
      el('#hit').setAttribute('disabled', 'disabled');
      el('#stay').setAttribute('disabled', 'disabled');

      // Hole-card wird aufgedeckt

      while (werteDealer < 17) { // #### while loop gefunden bei w3school ####  
        let zufall = Math.floor(Math.random() * startDeck.length); // zufällige Zahl, die zwischen 0 und der Länge des startDecks-Arrays liegen kann
        dealer.push(startDeck[zufall]); // zufällige Zahl wird als Index für das startDeck-Array genutzt, um einen beliebigen Wert aus dem Deck zu lesen und an das Array des Dealers zu übergeben


        let temp = startDeck[zufall]; // Inhalt aus Array mit Index 'zufall' wird Variable temp zugewiesen 
        werteDealer += wertAs(kartenSet[temp]['Wert'], werteDealer); // der Kartenwert wird ermittelt und an die Funktion wertAs übergeben; Rückgabewert wird mit werteDealer aufaddieren

        createKarte('img', 'card' + index + 'dealer', '', kartenSet[temp]['img'], '.playBoxDealer'); // stellt die dem Dealer zugewiesenen Karten auf dem Spieltisch dar
        index++;

        startDeck.splice(zufall, 1); // Wert wird aus startDeck gelöscht
        el('.ausgabeDeck').innerHTML = 'Noch ' + startDeck.length + ' Karten im Deck'; // gibt die verbliebene Anzahl an Karten im startDeck aus


      }
      el('#dealerpunkte').innerHTML = werteDealer;


      check(); // Spielzug des Dealers ist beendet; aufruf der der Funktion check, um die Karten des Spielers mit denen des Dealers zu vergleichen

    };

    function check() { // diese Funktion prüft die möglichen Spielausgange, berechnet den Gewinn und gibt alles im DOM aus
      //werteSpieler = 21;  // nur zum Testen benötigt
      //werteDealer = 21;  // nur zum Testen benötigt
      //console.log(dealer.length);

      if (werteDealer > 21) { // Dealer hat sich überkauft
        gewinn = einsatz; // Gewinn berechnen und gewinn im HTML ausgeben
        balance += einsatz + gewinn; // Gewinn zum Guthaben addieren

        // console.log('gewinn: ' + gewinn);      
        // console.log('balance: ' + balance); 

        el('#balance').innerHTML = 'Dein Gewinn ' + gewinn + ' $ <br> Balance: ' + balance + ' $';

        createButton('tryAgain flat', '<br>Dealer hat sich überkauft <br> Dein Gewinn ' + gewinn + ' $', 'Weiter');

        playSound(sounds.win);

        el('#overlay').addEventListener('click', function () {
          el('#overlay').remove();
          init();
        });
        return;
      };

      if (werteDealer > werteSpieler) { // Dealer hat mehr Punkte 

        if(dealer.length === 2 && werteDealer === 21){         // Dealer hat Blackjack
          
          createButton('tryAgain flat', '<br>Dealer hat Blackjack. <br> Dein Einsatz von <br>' + einsatz + ' $ ist futsch.', 'Weiter');
          
          playSound(sounds.dealerBj);


        }else{      // Dealer hat mehr Punkte

          createButton('tryAgain flat', '<br>Dealer hat mehr Punkte. <br> Dein Einsatz von <br>' + einsatz + ' $ ist futsch.', 'Weiter');

          playSound(sounds.loos);

        };


        el('#balance').innerHTML = 'Balance: ' + balance + ' $';
        el('#overlay').addEventListener('click', function () {
          el('#overlay').remove();
          init();
        });
        return;
      };

      if (werteDealer < werteSpieler) { // Dealer hat weniger Punkte
        if (spieler.length === 2 && werteSpieler === 21) { // Spieler hat Blackjack und Dealer hat 21 Punkte mit mehr als zwei Karten
          gewinn = Math.ceil(einsatz * 3 / 2); // Gewinn berechnen und aufrunden, um Dezimalzahlen zu vermeiden
          balance += einsatz + gewinn; // Gewinn und Einsatz zum Guthaben addieren

          // console.log('gewinn: ' + gewinn);      
          //  console.log('balance: ' + balance); 

          el('#balance').innerHTML = 'Dein Gewinn ' + gewinn + ' $ <br> Balance: ' + balance + ' $';

          createButton('tryAgain flat', '<br> <br>Spieler hat Blackjack <br> Dein Gewinn ' + gewinn + ' $', 'Weiter');

          playSound(sounds.playerBj);

          el('#overlay').addEventListener('click', function () {
            el('#overlay').remove();
            init();
          });

        } else {
          gewinn = einsatz; // Gewinn berechnen und gewinn im HTML ausgeben
          balance += einsatz + gewinn; // Gewinn und Einsatz zum Guthaben addieren

          // console.log('gewinn: ' + gewinn);      
          // console.log('balance: ' + balance); 

          el('#balance').innerHTML = 'Dein Gewinn ' + gewinn + ' $ <br> Balance: ' + balance + ' $';

          createButton('tryAgain flat', '<br>Dealer hat weniger Punkte <br> Dein Gewinn ' + gewinn + ' $', 'Weiter');

          playSound(sounds.win);

          el('#overlay').addEventListener('click', function () {
            el('#overlay').remove();
            init();
          });


        };
        return;
      };

      if (werteDealer = werteSpieler) { // Dealer und Spieler haben gleich viele Punkte

        if (dealer.length === 2 && spieler.length > 2 && werteDealer === 21) { // Dealer hat Blackjack und Spieler hat 21 Punkte mit mehr als zwei Karten
          el('#balance').innerHTML = 'Balance: ' + balance + ' $';

          createButton('tryAgain flat', 'Dealer hat Blackjack <br> Dein Einsatz von ' + einsatz + ' $ ist futsch.', 'Weiter');

          playSound(sounds.loos);

          el('#overlay').addEventListener('click', function () {
            el('#overlay').remove();
            init();
          });

          return;
        };

        if (spieler.length === 2 && dealer.length > 2 && werteSpieler === 21) { // Spieler hat Blackjack und Dealer hat 21 Punkte mit mehr als zwei Karten
          gewinn = Math.ceil(einsatz * 3 / 2); // Gewinn berechnen und aufrunden, um Dezimalzahlen zu vermeiden
          balance += einsatz + gewinn; // Gewinn und Einsatz zum Guthaben addieren

          //  console.log('gewinn: ' + gewinn);      
          // console.log('balance: ' + balance); 

          el('#balance').innerHTML = 'Dein Gewinn ' + gewinn + ' $ <br> Balance: ' + balance + ' $';

          createButton('tryAgain flat', 'Spieler hat Blackjack <br> Dein Gewinn ' + gewinn + ' $', 'Weiter');

          playSound(sounds.win);

          el('#overlay').addEventListener('click', function () {
            el('#overlay').remove();
            init();
          });

          return;
        };
        // Dealer und Spieler haben Blackjack oder gleiche Punktzahl
        balance += einsatz; // Einsatz zum verbliebenen Guthaben addieren
        el('#balance').innerHTML = 'Balance: ' + balance + ' $';

        createButton('tryAgain flat', '<br>Unentschieden! <br> Du bekommst deinen Einsatz zurück.', 'Weiter');

        playSound(sounds.draw);

        el('#overlay').addEventListener('click', function () {
          el('#overlay').remove();
          init();
        });

      };
    };




    function pleite() { // Prüfung ob noch Guthaben vorhanden ist und gewährt Kredit
      if (balance < 1) {
        createButton('tryAgain flat', '<br>Du bist pleite. <br>Wir geben Dir einen letzten Kredit von 20 $', 'Kredit mit Wucherzinsen akzeptieren'); // Erstellt Overlay mit Textausgabe und weiter-Button
        // console.log(el('#centerbtn'));
        el('#centerbtn').addEventListener('click', function () {
          el('#overlay').remove();
          balance = 20;
          el('#balance').innerHTML = 'Balance: ' + balance + ' $';
          el('#deinEinsatz').innerHTML = '<b>Bitte mach deinen Einsatz.</b> <br><br> Dein Einsatz: ' + einsatz + ' $ <br> Du kannst noch ' + balance + ' $ setzen.';

        });
      };
    };


    function einsatzBestaetigenButton(){
      let button = document.createElement('a');  // erzeugen eines HTML-Buttons, der innerhlab des overlays erscheint
        button.setAttribute('id', 'betset');
        // button.setAttribute('href', '#top');
        button.setAttribute('class', 'betBtn');
        button.innerHTML = 'Einsatz bestätigen und spielen.';
        el('.game').appendChild(button);


    };

    function einsatzMachen() {

        gebenStart(); // Spiel startet; Karten werden gegeben
        showHitStay(); // Buttons Hit und Stay werden klickbar
        //balance -= einsatz;                           // einsatz wird vom Guthaben abgezogen
        el('#balance').innerHTML = 'Dein Einsatz beträgt ' + einsatz + ' $ <br> Balance: ' + balance + ' $';
        el('#spielhinweis').innerHTML = 'Nimm mit HIT eine weitere Karte oder beende deinen Zug mit STAY.';

        el('#bet').setAttribute('disabled', 'disabled') // Einsatz-Button deaktivieren
      };
    // };


    //******************************************************/
    //******************** ZUWEISUNGEN  ********************/
    //******************************************************/


    init();

    // ### Event Listener auf das Drag 'n Drop der Jetons ###


    // für desktop mit drag
    el('#deinEinsatz').addEventListener('drop', drop);
    el('#deinEinsatz').addEventListener('dragover', allowDrop);

    el('#j1').addEventListener('dragstart', drag);
    el('#j5').addEventListener('dragstart', drag);
    el('#j10').addEventListener('dragstart', drag);
    el('#j20').addEventListener('dragstart', drag);
    el('#j50').addEventListener('dragstart', drag);


    // für mobile mit touch
    // el('#deinEinsatz').addEventListener('drop', drop);
    // el('#deinEinsatz').addEventListener('dragover',allowDrop);

    // el('#j1').addEventListener('touchmove',drag);


    // el('#j5').addEventListener('dragstart',drag);
    // el('#j10').addEventListener('dragstart',drag);
    // el('#j20').addEventListener('dragstart',drag);
    // el('#j50').addEventListener('dragstart',drag);

    // ### ENDE - Event Listener auf das Drag 'n Drop der Jetons ###


    el('#bet').addEventListener('click',function(){
      window.removeEventListener('scroll', noScrollDown);
      noScrollUp();
      window.addEventListener('scroll', noScrollUp);
    });

    

    el('#hit').addEventListener('click', spielerHand);
    el('#stay').addEventListener('click', dealerHand);


  
   
  }());