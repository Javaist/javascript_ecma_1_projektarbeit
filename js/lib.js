
// ####################### eigene libary  #######################

function el(css) {                  // HTML-Element-Selektor 
  return document.querySelector(css)
};


// erzeugt ein HTML-Element und legt es als Kind-Element in das angegebene Eltern-Element; dabei können ein paar Attribute gesetzt werden
function createKarte(ele,id,cla,src,css){  
let elem = document.createElement(ele);
elem.setAttribute('id', id);
elem.setAttribute('class', cla);
elem.setAttribute('src', src);
elem.setAttribute('height','190px');
elem.setAttribute('style','margin: 10px;');
console.log(elem);
el(css).appendChild(elem);


};
function createButton(clas,ptxt,btxt){
  el('#spielhinweis').innerHTML = '';     // Entfernt Spielhinweis aus Infobox

let overlay = document.createElement('div');  // erzeugen eines HTML-ELements, das sich über die Spieloberfläche legt und den Spieler über den Spielausgang informiert 
overlay.setAttribute('id', 'overlay');
overlay.setAttribute('class', clas);
overlay.setAttribute('value', 'Try Again');

let text = document.createElement('p');  // erzeugen eines HTML-p, der innerhlab des overlays erscheint und Text aufnehmen kann
text.setAttribute('id', 'overlay_txt');
text.setAttribute('class', 'overlay txt');
text.innerHTML = ptxt;

let button = document.createElement('button');  // erzeugen eines HTML-Buttons, der innerhlab des overlays erscheint
button.setAttribute('id', 'centerbtn');
button.setAttribute('class', 'btn');
button.innerHTML = btxt;

overlay.appendChild(text);                    // erzeugte HTML Element werden dem Eltern-Element zugewiesen
overlay.appendChild(button);
el('.optionBox').appendChild(overlay);
};


// ####################### ENDE #######################
